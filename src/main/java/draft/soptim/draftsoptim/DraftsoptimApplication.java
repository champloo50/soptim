package draft.soptim.draftsoptim;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DraftsoptimApplication {

	public static void main(String[] args) {
		SpringApplication.run(DraftsoptimApplication.class, args);
	}

}
