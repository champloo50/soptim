package draft.soptim.draftsoptim.repository;

import draft.soptim.draftsoptim.model.Adresse;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AddresseRepository extends JpaRepository<Adresse, Integer> {

}
