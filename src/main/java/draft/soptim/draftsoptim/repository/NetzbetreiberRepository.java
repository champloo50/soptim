package draft.soptim.draftsoptim.repository;

import draft.soptim.draftsoptim.model.Netzbetreiber;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface NetzbetreiberRepository extends JpaRepository<Netzbetreiber, Integer> {

}
