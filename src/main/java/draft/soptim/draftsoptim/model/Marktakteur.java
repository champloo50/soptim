package draft.soptim.draftsoptim.model;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "marktakteur")
@Inheritance(strategy = InheritanceType.JOINED)
public class Marktakteur {

    private Integer id;
    private String mastrNummer;
    private String personenart;
    private String firmenname;
    private String rechtsform;
    private String rechtsformSonstig;
    private String anrede;
    private String titel;
    private String vorname;
    private String nachname;
    private Adresse adresse;
    private Boolean istZustelladresse;
    private Adresse zustelladresse;
    private String abweichenderName;
    private String eMailAdresse;
    private String telefonnummer;
    private String fax;
    private String webAdresse;
    private String uStIdNr;
    private Date letzteAktualisierung;


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }


    @Column(name="mastrnummer")
    public String getMastrNummer() {
        return mastrNummer;
    }

    public void setMastrNummer(String mastrNummer) {
        this.mastrNummer = mastrNummer;
    }

    @Column(name = "personenart")
    public String getPersonenart() {
        return personenart;
    }

    public void setPersonenart(String personenart) {
        this.personenart = personenart;
    }

    @Column(name= "firmenname")
    public String getFirmenname() {
        return firmenname;
    }

    public void setFirmenname(String firmenname) {
        this.firmenname = firmenname;
    }

    @Column(name = "rechtsform")
    public String getRechtsform() {
        return rechtsform;
    }

    public void setRechtsform(String rechtsform) {
        this.rechtsform = rechtsform;
    }

    @Column(name = "rechtsformsonstig")
    public String getRechtsformSonstig() {
        return rechtsformSonstig;
    }

    public void setRechtsformSonstig(String rechtsformSonstig) {
        this.rechtsformSonstig = rechtsformSonstig;
    }

    @Column(name = "anrede")
    public String getAnrede() {
        return anrede;
    }

    public void setAnrede(String anrede) {
        this.anrede = anrede;
    }

    @Column(name = "titel")
    public String getTitel() {
        return titel;
    }

    public void setTitel(String titel) {
        this.titel = titel;
    }

    @Column(name = "vorname")
    public String getVorname() {
        return vorname;
    }

    public void setVorname(String vorname) {
        this.vorname = vorname;
    }

    @Column(name="nachname")
    public String getNachname() {
        return nachname;
    }

    public void setNachname(String nachname) {
        this.nachname = nachname;
    }

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "adresseid")
    public Adresse getAdresse() {
        return adresse;
    }

    public void setAdresse(Adresse adresse) {
        this.adresse = adresse;
    }

    @Column(name = "istzustelladresse")
    public Boolean isIstZustelladresse() {
        return istZustelladresse;
    }

    public void setIstZustelladresse(Boolean istZustelladresse) {
        this.istZustelladresse = istZustelladresse;
    }

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "zustelladresseid")
    public Adresse getZustelladresse() {
        return zustelladresse;
    }

    public void setZustelladresse(Adresse zustelladresse) {
        this.zustelladresse = zustelladresse;
    }

    @Column(name = "abweichendername")
    public String getAbweichenderName() {
        return abweichenderName;
    }

    public void setAbweichenderName(String abweichenderName) {
        this.abweichenderName = abweichenderName;
    }

    @Column(name = "emailadresse")
    public String geteMailAdresse() {
        return eMailAdresse;
    }

    public void seteMailAdresse(String eMailAdresse) {
        this.eMailAdresse = eMailAdresse;
    }

    @Column(name = "telefonnummer")
    public String getTelefonnummer() {
        return telefonnummer;
    }

    public void setTelefonnummer(String telefonnummer) {
        this.telefonnummer = telefonnummer;
    }

    @Column(name = "fax")
    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    @Column(name = "webadresse")
    public String getWebAdresse() {
        return webAdresse;
    }

    public void setWebAdresse(String webAdresse) {
        this.webAdresse = webAdresse;
    }

    @Column(name = "ustidnr")
    public String getuStIdNr() {
        return uStIdNr;
    }

    public void setuStIdNr(String uStIdNr) {
        this.uStIdNr = uStIdNr;
    }

    @Column(name = "letzteaktualisierung")
    public Date getLetzteAktualisierung() {
        return letzteAktualisierung;
    }

    public void setLetzteAktualisierung(Date letzteAktualisierung) {
        this.letzteAktualisierung = letzteAktualisierung;
    }
}
