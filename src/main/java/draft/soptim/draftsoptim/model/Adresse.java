package draft.soptim.draftsoptim.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;

@Entity
@Table(name = "adresse")
@JsonIdentityInfo(generator= ObjectIdGenerators.PropertyGenerator.class, property="id", scope = Adresse.class)
public class Adresse {

    private Integer id;
    private String land;
    private String ort;
    private String plz;
    private String strasse;
    private String hausnummer;
    private String addresszusatz;



    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name = "land")
    public String getLand() {
        return land;
    }

    public void setLand(String land) {
        this.land = land;
    }

    @Column(name = "ort")
    public String getOrt() {
        return ort;
    }

    public void setOrt(String ort) {
        this.ort = ort;
    }

    @Column(name = "plz")
    public String getPlz() {
        return plz;
    }

    public void setPlz(String plz) {
        this.plz = plz;
    }

    @Column(name = "strasse")
    public String getStrasse() {
        return strasse;
    }

    public void setStrasse(String strasse) {
        this.strasse = strasse;
    }

    @Column(name = "hausnummer")
    public String getHausnummer() {
        return hausnummer;
    }

    public void setHausnummer(String hausnummer) {
        this.hausnummer = hausnummer;
    }

    @Column(name = "adresszusatz")
    public String getAddresszusatz() {
        return addresszusatz;
    }

    public void setAddresszusatz(String addresszusatz) {
        this.addresszusatz = addresszusatz;
    }
}
