package draft.soptim.draftsoptim.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "netzbetreiber")
public class Netzbetreiber extends Marktakteur {

    private String mastrNummer;
    private String betriebsnummerBNetzA;
    private String marktrollen;

    @Override
    @Column(name = "mastrnummer")
    public String getMastrNummer() {
        return mastrNummer;
    }

    @Override
    public void setMastrNummer(String mastrNummer) {
        this.mastrNummer = mastrNummer;
    }

    @Column(name = "betriebsnummerbnetza")
    public String getBetriebsnummerBNetzA() {
        return betriebsnummerBNetzA;
    }

    public void setBetriebsnummerBNetzA(String betriebsnummerBNetzA) {
        this.betriebsnummerBNetzA = betriebsnummerBNetzA;
    }

    @Column(name = "marktrollen")
    public String getMarktrollen() {
        return marktrollen;
    }

    public void setMarktrollen(String marktrollen) {
        this.marktrollen = marktrollen;
    }
}
