package draft.soptim.draftsoptim.converter;

import draft.soptim.draftsoptim.dtos.AdresseDto;
import draft.soptim.draftsoptim.model.Adresse;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface AdresseConverter {

    AdresseConverter INSTANCE = Mappers.getMapper(AdresseConverter.class );

    AdresseDto adresseToDto(Adresse adresse);

    Adresse dtoToAdresse(AdresseDto adresseDto);
}
