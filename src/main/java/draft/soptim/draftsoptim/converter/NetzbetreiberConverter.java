package draft.soptim.draftsoptim.converter;

import draft.soptim.draftsoptim.dtos.NetzbetreiberDto;
import draft.soptim.draftsoptim.model.Netzbetreiber;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface NetzbetreiberConverter {

    NetzbetreiberConverter INSTANCE = Mappers.getMapper(NetzbetreiberConverter.class );

    NetzbetreiberDto netzbetreiberToDto(Netzbetreiber netzbetreiber);

    Netzbetreiber dtoToNetzbetreiber(NetzbetreiberDto netzbetreiberDto);
}
