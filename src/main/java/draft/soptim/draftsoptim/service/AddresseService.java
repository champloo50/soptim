package draft.soptim.draftsoptim.service;

import draft.soptim.draftsoptim.converter.AdresseConverter;
import draft.soptim.draftsoptim.dtos.AdresseDto;
import draft.soptim.draftsoptim.model.Adresse;
import draft.soptim.draftsoptim.repository.AddresseRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class AddresseService {


    private AddresseRepository addresseRepository;

    public AddresseService(AddresseRepository addresseRepository) {
        this.addresseRepository = addresseRepository;
    }

    public List<AdresseDto> getAll() {
        return addresseRepository.findAll().stream()
                .map(AdresseConverter.INSTANCE::adresseToDto).collect(Collectors.toList());
    }

    public AdresseDto getAdresse(int id) {
        return addresseRepository.findById(id).map(AdresseConverter.INSTANCE::adresseToDto).orElse(new AdresseDto());
    }

    public void deleteAdresse(int id) {
        addresseRepository.deleteById(id);
    }

    public AdresseDto addAdresse(AdresseDto adresseDto) {
        Adresse savedAdresse = addresseRepository.save(AdresseConverter.INSTANCE.dtoToAdresse(adresseDto));
        return AdresseConverter.INSTANCE.adresseToDto(savedAdresse);
    }
}
