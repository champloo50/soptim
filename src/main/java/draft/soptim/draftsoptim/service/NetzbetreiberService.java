package draft.soptim.draftsoptim.service;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import draft.soptim.draftsoptim.converter.NetzbetreiberConverter;
import draft.soptim.draftsoptim.dtos.NetzbetreiberDto;
import draft.soptim.draftsoptim.model.Netzbetreiber;
import draft.soptim.draftsoptim.repository.NetzbetreiberRepository;
@Service
public class NetzbetreiberService {

    private NetzbetreiberRepository netzbetreiberRepository;

    public NetzbetreiberService(NetzbetreiberRepository netzbetreiberRepository) {
        this.netzbetreiberRepository = netzbetreiberRepository;
    }

    public List<NetzbetreiberDto> getAllNetzbetreiber() {
        return netzbetreiberRepository.findAll()
                .stream().map(NetzbetreiberConverter.INSTANCE::netzbetreiberToDto).collect(Collectors.toList());
    }

	public NetzbetreiberDto getNetzbetreiber(Integer netzbetreiberId) {
		return  netzbetreiberRepository.findById(netzbetreiberId).map(NetzbetreiberConverter.INSTANCE::netzbetreiberToDto).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
	}

	public NetzbetreiberDto addNetzbetreiber(NetzbetreiberDto netzbetreiberDto) {
        Netzbetreiber savedNetzbetreiber = netzbetreiberRepository.save(NetzbetreiberConverter.INSTANCE.dtoToNetzbetreiber(netzbetreiberDto));
        return NetzbetreiberConverter.INSTANCE.netzbetreiberToDto(savedNetzbetreiber);
	}

    public void deleteNetzbetreiber(Integer netzbetreiberId) {
        netzbetreiberRepository.deleteById(netzbetreiberId);
    }
}
