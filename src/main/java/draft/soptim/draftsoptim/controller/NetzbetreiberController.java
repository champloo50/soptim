package draft.soptim.draftsoptim.controller;

import java.util.List;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import draft.soptim.draftsoptim.dtos.NetzbetreiberDto;

@RequestMapping("/netzbetreiber")
public interface NetzbetreiberController {

    @RequestMapping(value = "", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<List<NetzbetreiberDto>> getAllNetzbetreiber();

    @RequestMapping(value = "/{netzbetreiberId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<NetzbetreiberDto> getNetzbetreiber(@PathVariable Integer netzbetreiberId);

    @RequestMapping(value = "", method = RequestMethod.POST, consumes=MediaType.APPLICATION_JSON_VALUE,produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<NetzbetreiberDto> addNetzbetreiber(@RequestBody final NetzbetreiberDto netzbetreiberDto);

    @RequestMapping(value = "/{netzbetreiberId}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<NetzbetreiberDto> deleteNetzbetreiber(@PathVariable Integer netzbetreiberId);


}   
