package draft.soptim.draftsoptim.controller.impl;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import draft.soptim.draftsoptim.controller.NetzbetreiberController;
import draft.soptim.draftsoptim.dtos.NetzbetreiberDto;
import draft.soptim.draftsoptim.service.NetzbetreiberService;

@RestController
public class NetzbetreiberControllerImpl implements NetzbetreiberController {

    private static final Logger log = LogManager.getLogger(NetzbetreiberControllerImpl.class);
    private NetzbetreiberService netzbetreiberService;

    public NetzbetreiberControllerImpl(NetzbetreiberService netzbetreiberService) {
        this.netzbetreiberService = netzbetreiberService;
    }

    @Override
    public ResponseEntity<List<NetzbetreiberDto>> getAllNetzbetreiber() {
        try {
            return ResponseEntity.status(HttpStatus.OK)
                    .body(netzbetreiberService.getAllNetzbetreiber());
        } catch (final Exception e) {
            log.error(e);
            throw e;
        }
    }

    @Override
    public ResponseEntity<NetzbetreiberDto> getNetzbetreiber(Integer netzbetreiberId) {
        try {
            return ResponseEntity.status(HttpStatus.OK)
                    .body(netzbetreiberService.getNetzbetreiber(netzbetreiberId));
        }
        catch (final Exception e) {
            log.error(e);
            throw e;
        }
    }

    @Override
    public ResponseEntity<NetzbetreiberDto> addNetzbetreiber(NetzbetreiberDto netzbetreiberDto) {
        try {
            return ResponseEntity.status(HttpStatus.OK)
                    .body(netzbetreiberService.addNetzbetreiber(netzbetreiberDto));
        } catch (final Exception e) {
            log.error(e);
            throw e;
        }
    }

    @Override
    public ResponseEntity<NetzbetreiberDto> deleteNetzbetreiber(Integer netzbetreiberId) {
        try {
            netzbetreiberService.deleteNetzbetreiber(netzbetreiberId);
            return ResponseEntity.status(HttpStatus.OK).build();
        } catch (final Exception e) {
            log.error(e);
            throw e;
        }
    }

}
