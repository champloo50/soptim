package draft.soptim.draftsoptim.controller.impl;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import draft.soptim.draftsoptim.controller.AdresseController;
import draft.soptim.draftsoptim.dtos.AdresseDto;
import draft.soptim.draftsoptim.service.AddresseService;

@RestController
public class AdresseControllerImpl implements AdresseController {

    private static final Logger log = LogManager.getLogger(AdresseControllerImpl.class);
    private AddresseService addresseService;

    public AdresseControllerImpl(AddresseService addresseService) {
        this.addresseService = addresseService;
    }

    @Override
    public ResponseEntity<List<AdresseDto>> getAllAdressen() {
        try {
            return ResponseEntity.status(HttpStatus.OK)
                    .body(addresseService.getAll());
        } catch (final Exception e) {
            log.error(e);
            throw e;
        }
    }

    @Override
    public ResponseEntity<AdresseDto> getAdresse(int id) {
        try {
            return ResponseEntity.status(HttpStatus.OK)
                    .body(addresseService.getAdresse(id));
        } catch (final Exception e) {
            log.error(e);
            throw e;
        }
    }

    @Override
    public ResponseEntity<AdresseDto> addAdresse(AdresseDto adresseDto) {
        try {
            return ResponseEntity.status(HttpStatus.OK)
                    .body(addresseService.addAdresse(adresseDto));
        } catch (final Exception e) {
            log.error(e);
            throw e;
        }
    }

}
