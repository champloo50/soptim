package draft.soptim.draftsoptim.dtos;

import draft.soptim.draftsoptim.model.Adresse;
import lombok.Data;

import java.util.Date;

@Data
public class MarktakteurDto {

    private Integer id;
    private String mastrNummer;
    private String personenart;
    private String firmenname;
    private String rechtsform;
    private String rechtsformSonstig;
    private String anrede;
    private String titel;
    private String vorname;
    private String nachname;
    private Adresse adresse;
    private Boolean istZustelladresse;
    private Adresse zustelladresse;
    private String abweichenderName;
    private String eMailAdresse;
    private String telefonnummer;
    private String fax;
    private String webAdresse;
    private String uStIdNr;
    private Date letzteAktualisierung;

}
