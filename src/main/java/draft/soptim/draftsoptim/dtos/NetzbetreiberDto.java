package draft.soptim.draftsoptim.dtos;

import lombok.Data;

@Data
public class NetzbetreiberDto extends MarktakteurDto {

    private String mastrNummer;
    private String betriebsnummerBNetzA;
    private String marktrollen;
}
