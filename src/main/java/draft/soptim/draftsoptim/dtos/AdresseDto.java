package draft.soptim.draftsoptim.dtos;

import lombok.Data;

@Data
public class AdresseDto {

    private Integer id;
    private String land;
    private String ort;
    private String plz;
    private String strasse;
    private String hausnummer;
    private String addresszusatz;


}
