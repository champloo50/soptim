INSERT INTO ADRESSE VALUES(
  1,
  'Deutschland',
  '47229',
  'Duisburg',
  'Hellmanstr',
  '49',
  ''
);

INSERT INTO ADRESSE VALUES(
  2,
  'Italien',
  '478AB',
  'Jenova',
  'sphagetti',
  '10',
  ''
);

INSERT INTO ADRESSE VALUES(
  3,
  'Deutschland',
  '45894',
  'ESSEN',
  'Hell',
  '20',
  ''
);

INSERT INTO ADRESSE VALUES(
  4,
  'Deutschland',
  '17227',
  'BERLIN',
  'Berlinerstr',
  '999',
  ''
);

INSERT INTO MARKTAKTEUR (ID , VORNAME , ADRESSEID , ZUSTELLADRESSEID) VALUES(
  1,
  'Hakan',
  1,
  2
);

INSERT INTO MARKTAKTEUR (ID , VORNAME , ADRESSEID , ZUSTELLADRESSEID) VALUES(
  2,
  'Daniel',
  3,
  4
);

INSERT INTO MARKTAKTEUR (ID , VORNAME , ADRESSEID , ZUSTELLADRESSEID) VALUES(
  3,
  'Detlef',
  4,
  3
);

INSERT INTO NETZBETREIBER (ID, mastrNummer) VALUES(
  1,
  '123'
);

INSERT INTO NETZBETREIBER (ID, mastrNummer) VALUES(
  2,
  '345'
);

INSERT INTO ANLAGENBETREIBER (ID) VALUES(
 3
);
