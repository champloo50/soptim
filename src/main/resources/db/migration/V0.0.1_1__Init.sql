CREATE TABLE ADRESSE (
  id SERIAL PRIMARY KEY NOT NULL,
  land VARCHAR(100),
  plz VARCHAR(100),
  ort VARCHAR(100),
  strasse VARCHAR(100),
  hausnummer VARCHAR(100),
  adresszusatz VARCHAR(100)
);

CREATE TABLE MARKTAKTEUR (
  id SERIAL PRIMARY KEY NOT NULL,
  mastrNummer VARCHAR(100),
  personenart VARCHAR(100),
  firmenname VARCHAR(100),
  rechtsform VARCHAR(100),
  rechtsformSonstig VARCHAR(100),
  anrede VARCHAR(100),
  titel VARCHAR(100),
  vorname VARCHAR(150),
  nachname VARCHAR(150),
  adresseId INTEGER NOT NULL UNIQUE,
  istZustelladresse BOOLEAN,
  zustelladresseId INTEGER UNIQUE,
  abweichenderName VARCHAR(100),
  eMailAdresse VARCHAR(256),
  telefonnummer VARCHAR(100),
  fax VARCHAR(100),
  webAdresse VARCHAR(100),
  uStIdNr VARCHAR(100),
  letzteAktualisierung TIMESTAMP,
  CONSTRAINT marktaktuer_adresse_reference FOREIGN KEY (adresseId) REFERENCES adresse(id),
  CONSTRAINT marktaktuer_zustelladresse_reference FOREIGN KEY (zustelladresseId) REFERENCES adresse(id)
);


CREATE TABLE NETZBETREIBER(
  id SERIAL PRIMARY KEY NOT NULL,
  mastrNummer VARCHAR(100),
  betriebsnummerBNetzA VARCHAR(100),
  marktrollen VARCHAR(100),
  CONSTRAINT netzbetreiber_marktaktuer_reference FOREIGN KEY (id) REFERENCES marktakteur(id)
);

CREATE TABLE ANLAGENBETREIBER(
  id SERIAL PRIMARY KEY NOT NULL,
  CONSTRAINT anlagenbetreiber_marktaktuer_reference FOREIGN KEY (id) REFERENCES marktakteur(id)
);
